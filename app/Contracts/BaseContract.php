<?php

namespace App\Contracts;

interface BaseContract
{

    /**
     * @return mixed
     */
    public function create(array $arr);

    /**
     * @return mixed
     */
    public function update(array $arr, int $id);

    /**
     * @return mixed
     */
    public function all(array $fields = [], $columns = array("*"), string $orderBy = "id", string $sortBy = "desc");

    /**
     * @return mixed
     */
    public function find(int $id);
    
    /**
     * @return mixed
     */
    public function findOrFail(int $id);
    
    /**
     * @return mixed
     */
    public function findBy(array $data);
    
    /**
     * @return mixed
     */
    public function findOneBy(array $data);
    
    /**
     * @return mixed
     */
    public function findOneByOrFail(array $data);
    
    /**
     * @return mixed
     */
    public function delete(int $id);
}
