<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    private $options;

    public function __construct()
    {
        $this->options = new \Iyzipay\Options();
        $this->options->setApiKey("sandbox-jY4xxb3Y5C0Vs2xap1KeDtJ8J0ubfRfA");
        $this->options->setSecretKey("sandbox-2QSVdibX3QoLKfqEI8OMMfb3TJhQkJHN");
        $this->options->setBaseUrl("https://sandbox-api.iyzipay.com");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("test");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $data
     * @return \Illuminate\Http\Response
     */
    public function store(Request $data)
    {

        $basketItems = array();

        $request = new \Iyzipay\Request\CreatePaymentRequest();
        $request->setLocale(\Iyzipay\Model\Locale::EN);
        $request->setConversationId("123456789");
        $request->setPrice("1");
        $request->setPaidPrice("1");
        $request->setCurrency(\Iyzipay\Model\Currency::TL);
        $request->setInstallment(1);
        $request->setBasketId("B67832");
        $request->setPaymentChannel(\Iyzipay\Model\PaymentChannel::WEB);
        $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
        $request->setCallbackUrl("http://localhost:8000/test/PROCCESSING");

        $paymentCard = new \Iyzipay\Model\PaymentCard();
        $paymentCard->setCardHolderName("John Doe");
        $paymentCard->setCardNumber("5528790000000008");
        $paymentCard->setExpireMonth("12");
        $paymentCard->setExpireYear("2030");
        $paymentCard->setCvc("123");
        $paymentCard->setRegisterCard(0);
        $request->setPaymentCard($paymentCard);

        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId("BY789");
        $buyer->setName("John");
        $buyer->setSurname("Doe");
        $buyer->setGsmNumber("+905350000000");
        $buyer->setEmail("email@email.com");
        $buyer->setIdentityNumber("74300864791");
        $buyer->setLastLoginDate("2015-10-05 12:43:35");
        $buyer->setRegistrationDate("2013-04-21 15:12:09");
        $buyer->setRegistrationAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
        $buyer->setIp("85.34.78.112");
        $buyer->setCity("Istanbul");
        $buyer->setCountry("Turkey");
        $buyer->setZipCode("34732");
        $request->setBuyer($buyer);

        $shippingAddress = new \Iyzipay\Model\Address();
        $shippingAddress->setContactName("Jane Doe");
        $shippingAddress->setCity("Istanbul");
        $shippingAddress->setCountry("Turkey");
        $shippingAddress->setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
        $shippingAddress->setZipCode("34742");
        $request->setShippingAddress($shippingAddress);
        $billingAddress = new \Iyzipay\Model\Address();
        $billingAddress->setContactName("Jane Doe");
        $billingAddress->setCity("Istanbul");
        $billingAddress->setCountry("Turkey");
        $billingAddress->setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
        $billingAddress->setZipCode("34742");
        $request->setBillingAddress($billingAddress);

        $BasketItem = new \Iyzipay\Model\BasketItem();
        $BasketItem->setId("BI101");
        $BasketItem->setName("Binocular");
        $BasketItem->setCategory1("Collectibles");
        $BasketItem->setItemType(\Iyzipay\Model\BasketItemType::VIRTUAL);
        $BasketItem->setPrice("1");
        $basketItems[] = $BasketItem;

        $request->setBasketItems($basketItems);

        $payment = \Iyzipay\Model\ThreedsInitialize::create($request, $this->options);
        $r = json_decode($payment->getrawResult(), true);

        return base64_decode($r['threeDSHtmlContent']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  Request  $data
     * @return \Illuminate\Http\Response
     */
    public function show(Request $data, $id)
    {

        if ($id !== "PROCCESSING")
            return "INVALID REQUEST!";

        $request = new \Iyzipay\Request\CreateThreedsPaymentRequest();
        $request->setLocale(\Iyzipay\Model\Locale::EN);
        $request->setConversationId($data['conversationId']);
        $request->setPaymentId($data['paymentId']);
        $request->setConversationData($data['conversationData']);

        $threedsPayment = \Iyzipay\Model\ThreedsPayment::create($request, $this->options);
        $r = json_decode($threedsPayment->getRawResult(), true);

        if ($r['status'] === "success") {
            echo "Successfully";
        } else {
            echo "Error: " . $r['errorMessage'];
        }

        echo "\n\n";
        return $r;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
