<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->middleware("guest")->except(['Logout']);
        $this->userRepository = $userRepository;
    }

    /**
     * @param LoginRequest $request
     * @return mixed
     */

    public function Login(LoginRequest $request)
    {
        $data = $request->validated();
        
        if (Auth::attempt(['username' => $data['username'], 'password' => $data['password']], true)) {
            request()->session()->regenerate();
            return redirect()->route('home')->with("alerts", [array('status' => "success", "text" => "Welcome back")]);
        }

        return back()->with("alerts", [array('status' => "danger", "text" => "Username or Password is wrong!")]);
    }

    /**
     * @param RegisterRequest $request 
     * @return mixed
     */

    public function Register(RegisterRequest $request)
    {
        $data = $request->validated();
        
        $user = $this->userRepository->createUser([
            'username' => $data['username'],
            'password' => Hash::make($data['password'])
        ]);
        
        Auth::login($user, true);

        return redirect()->route('home')->with("alerts", [['status' => "success", "text" => "Successfully!"]]);
    }

    public function Logout() {
        Auth::logout();
        return back();
    }
}
