<?php

namespace App\Repositories;

use App\Contracts\BaseContract;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository.
 */
class BaseRepository implements BaseContract
{

    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }


    public function create(array $arr)
    {
        return $this->model->create($arr);
    }

    public function update(array $arr, int $id)
    {
        return $this->model->find($id)->update($arr);
    }

    public function all(array $fields = [], $columns = array("*"), string $orderBy = "id", string $sortBy = "desc")
    {
        return $this->model->where($fields)->orderBy($orderBy, $sortBy)->get($columns);
    }

    public function find(int $id)
    {
        return $this->model->find($id);
    }

    public function findOrFail(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function findBy(array $data)
    {
        return $this->model->where($data)->all();
    }

    public function findOneBy(array $data)
    {
        return $this->model->where($data)->first();
    }

    public function findOneByOrFail(array $data)
    {
        return $this->model->where($data)->firstOrFail();
    }

    public function delete(int $id)
    {
        return $this->model->find($id)->delete();
    }
}
