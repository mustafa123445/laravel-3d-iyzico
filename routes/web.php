<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Payment\PaymentController;
use Illuminate\Support\Facades\Route;
use Symfony\Component\VarDumper\Caster\RedisCaster;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', function () {
    return redirect()->route('home');
});

Route::any('/home', function () {
    return view('welcome');
})->name('home');

Route::middleware(['guest'])->group(function () {

    Route::get("/login", function () {
        return view('auth.login');
    })->name('login');

    Route::post("/login", [AuthController::class, "Login"]);


    Route::get("/register", function () {
        return view('auth.register');
    })->name('register');

    Route::post("/register", [AuthController::class, "Register"]);
});

Route::resource("/test", PaymentController::class);
Route::post("/test/{id}", [PaymentController::class, "show"])->name("test");


Route::middleware(['auth'])->group(function () {
    Route::any("/logout", [AuthController::class, "Logout"]);
});
