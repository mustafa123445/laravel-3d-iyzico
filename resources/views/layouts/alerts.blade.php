@if (@count(session('alerts')))
    <div class="col-12 mt-2">
        @foreach (session('alerts') as $alert)
            <div style="cursor: pointer;" class="alert alert-{{ $alert['status'] }}" onclick="$(this).remove();">
                <strong>{{ ucfirst($alert['status']) }}!</strong><br>
                {{ $alert['text'] }}
            </div>
        @endforeach
    </div>
@endif

@if (@count($errors->any()))
    <div class="col-12 mt-2">
        @foreach ($errors->all() as $error)
            <div style="cursor: pointer;" class="alert alert-danger" onclick="$(this).remove();">
                <strong>Danger!</strong><br>
                {{ $error }}
            </div>
        @endforeach
    </div>
@endif
