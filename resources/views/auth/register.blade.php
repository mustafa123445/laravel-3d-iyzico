@extends('main')
@section('title', 'Register')

@section('body')
    <div class="row text-light">
        <div class="col-12">
            <h1>Register</h1>
            <form method="POST">
                @csrf
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" name="username" id="username" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-light">Register</button>
                </div>
            </form>
        </div>
    </div>
@endsection
